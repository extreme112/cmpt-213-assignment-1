package com.patricksanjuan;

/**
 * This class implements a new Minion object with a certain name and height.
 * The number of evil deeds completed by a Minion is initially 0.
 * @author Patrick San Juan
 */
public class Minion {
    private String name;
    private double height;
    private int evilDeedsCompleted = 0;

    /**
     * This constructs a Minion with a specified name and height
     * @param name the name of the minion
     * @param height the height of the minion
     */
    Minion(String name, double height){
        this.name = name;
        this.height = height;
    }

    /**
     * Returns the name of the minion.
     * @return name of the minion
     */
    public String getName() {
        return name;
    }

    /**
     * Returns height of the minion.
     * @return height of the minion
     */
    public double getHeight() {
        return height;
    }

    /**
     * Returns the number of evil deeds the minion has completed.
     * @return number of evil deeds the minion completed
     */
    public int getEvilDeedsCompleted() {
        return evilDeedsCompleted;
    }

    /**
     * Increments the number of evil deeds completed
     */
    public void incrementEvilDeeds(){
        evilDeedsCompleted++;
    }

    /**
     * Returns the package name, class name, minion name, evil, deeds completed, and height of the minion.
     * @return String with the package name, class name, minion name, evil, deeds completed, and height of the minion.
     */
    @Override
    public String toString() {
        return getClass().getName() + "[Name: " + name + ", Evil Deeds: " + evilDeedsCompleted + ", Height: " + height + "]";
    }
}
