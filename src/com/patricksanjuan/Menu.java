package com.patricksanjuan;

import java.util.List;
import java.util.Scanner;

/**
 * A Menu object with a title and list of options or list of minions.
 * This class can handles display and user input.
 */
public class Menu {
    private String title;
    private String[] options;
    private List<Minion> minions;

    /**
     * This constructs a menu with a title and a list of options
     * @param title the tile you want to display
     * @param options a String array of options
     */
    public Menu(String title, String[] options) {
        this.title = title;
        this.options = options;
    }

    /**
     * This constructs a menu with a title and a list
     * of Minions
     * @param title the title you want to display
     * @param minions the List of Minions you pass in
     */
    public Menu(String title, List<Minion> minions) {
        this.title = title;
        this.minions = minions;
    }

    /**
     * Wrapper for class methods displayMenuTitle and displayOptions
     */
    public void display(){
        displayMenuTitle();
        displayOptions();
    }

    /**
     * Repeatedly the user to enter an integer number between
     * the min and max parameters
     * @param min the minimum value the user can input
     * @param max the maximum value the user can input
     * @return user input as an integer between min and max parameters
     */
    public static int getIntBetween(int min, int max){
        System.out.print("Enter a number between " + min + " and " + max + ": ");
        int userInput = getIntFromUser();
        while (userInput > max || userInput < min){
            System.out.print("Error: Enter a valid number: ");
            userInput = getIntFromUser();
        }
        return userInput;
    }

    /**
     * Prompts the user for a number (double) with a message displayed
     * to display passed in
     * @param message the message to display to the user
     * @return user input as a double
     */
    public static double getDoubleFromUser(String message){
        System.out.println(message);
        Scanner reader;
        Double value = null;
        while (value == null){
            try {
                reader = new Scanner(System.in);
                value = reader.nextDouble();
            } catch (Exception e){
                e.getMessage();
                System.out.println("You did not enter a number. Enter a number:");
            }
        }
        return value;
    }

    /**
     * Gets an number (int) from the user
     * @return user input as an integer
     */
    public static int getIntFromUser() {
        Scanner reader;
        Integer value = null;
        while (value == null){
            try {
                reader = new Scanner(System.in);
                value = reader.nextInt();
            } catch (Exception e){
                e.getMessage();
                System.out.println("You did not enter a number. Enter a number:");
            }
        }
        return value;
    }

    /**
     * Prompts the user for input (String) with a message displayed
     * @param message the message to display to the user
     * @return user input as a String
     */
    public static String getStringFromUser(String message) {
        System.out.println(message);
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }

    /**
     * Displays the menu title with a star of boxes surrounding it
     */
    private void displayMenuTitle(){
        String rowOfStars = "*";
        for(int i = 0; i < title.length() + 1; i++) {
            rowOfStars = rowOfStars + "*";
        }
        System.out.println(rowOfStars);
        System.out.println("*" + title + "*");
        System.out.println(rowOfStars);
    }

    /**
     * Displays a list of options or a list of minions,
     * depending on the type of the parameters passed in
     * the constructor. If a list of minions is passed, the index number
     * of the minion is incremented by 1 for a more typical
     * use experience.
     */
    private void displayOptions(){
        if(options != null){
            for(int i = 0; i < options.length; i++){
                System.out.println((i+1) + ". " + options[i]);
            }
        } else if (minions != null){
            for (int i = 0; i < minions.size(); i++) {
                System.out.println((i+1) + ". " + minions.get(i).getName() + ", " + minions.get(i).getHeight() + "m, " + minions.get(i).getEvilDeedsCompleted());
            }
        }
    }
}
