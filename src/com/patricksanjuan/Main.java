package com.patricksanjuan;

import java.util.ArrayList;
import java.util.List;

/**
 * Entry point for the program. Handles logic on user input.
 */
public class Main {
    private static final int MIN_USER_INPUT = 0;
    // Since Menu displays Minion's index + 1 when listing minions,
    // a user input of 0 will result in an index value of -1
    private static final int MIN_USER_INPUT_COMPENSATED = MIN_USER_INPUT - 1;
    private static final String[] MENU_OPTIONS = {"List of Minions","Add Minion","Remove Minion","Add Evil Deed to Minion","Debug: Dump Object (toString)","Exit"};

    /**
     * Entry point of program.
     * @param args Not needed.
     */
    public static void main(String args[]){
        List<Minion> minions = new ArrayList<>();
        initializeMinions(minions);
        boolean exitFlag = false;

        while (!exitFlag){
            Menu mainMenu = new Menu("Main Menu", MENU_OPTIONS);
            mainMenu.display();
            int optionSelected = Menu.getIntBetween(MIN_USER_INPUT + 1, MENU_OPTIONS.length);

            switch (optionSelected){
                case 1: listMinions(minions); break;
                case 2: addNewMinion(minions); break;
                case 3: removeMinion(minions); break;
                case 4: addEvilDeed(minions); break;
                case 5: debug(minions); break;
                case 6: exitFlag = true; break;
                default: System.out.println("Invalid selection");
            }
        }
    }

    /**
     * Displays the list of minions
     * @param minions List of minions.
     */
    private static void listMinions(List<Minion> minions){
        Menu minionListMenu = new Menu(MENU_OPTIONS[0],minions);
        minionListMenu.display();
    }

    /**
     * Prompts the user to enter a name and height, then adds a the newly
     * generated minion to the passed parameter
     * @param minions List of minions.
     */
    private static void addNewMinion(List<Minion> minions){
        Menu addNewMinionMenu = new Menu(MENU_OPTIONS[1], new String[]{});
        addNewMinionMenu.display();
        String minionName = Menu.getStringFromUser("Enter the minions name: ");
        double minionHeight = Menu.getDoubleFromUser("Enter the minions height: ");

        while (minionHeight <= 0){
            System.out.println("Minion height cannot be less than or equal to 0. Enter a non-negative number. ");
            minionHeight = Menu.getDoubleFromUser("Enter the minions height: ");
        }
        minions.add(new Minion(minionName, minionHeight));
    }

    /**
     * Displays the list of minions in "minions', then prompts
     * the user to delete one of them.
     * @param minions List of minions.
     */
    private static void removeMinion(List<Minion> minions){
        Menu removeMinionMenu = new Menu(MENU_OPTIONS[2], minions);
        removeMinionMenu.display();
        System.out.println("Enter the number of the minion you want to remove: (Enter 0 to Exit)");
        int userInput = Menu.getIntBetween(MIN_USER_INPUT, minions.size()) - 1;

        if (userInput != MIN_USER_INPUT_COMPENSATED){
            minions.remove(userInput);
        }
    }

    /**
     * Displays the list of minions in "minions', then prompts
     * the user to select one
     * @param minions List of minions.
     */
    private static void addEvilDeed(List<Minion> minions){
        Menu addEvilDeedMenu = new Menu(MENU_OPTIONS[3], minions);
        addEvilDeedMenu.display();
        System.out.println("Enter the number of the minion you want to add evil deeds to: (Enter 0 to Exit)");
        int userInput = Menu.getIntBetween(MIN_USER_INPUT, minions.size()) - 1;
        if (userInput != MIN_USER_INPUT_COMPENSATED){
            minions.get(userInput).incrementEvilDeeds();
        }
    }

    /**
     * Displays more detail about the minions
     * @param minions List of minions.
     */
    private static void debug(List<Minion> minions){
        Menu debugMenu = new Menu(MENU_OPTIONS[4],new String[]{});
        debugMenu.display();
        for (int i = 0; i < minions.size(); i++) {
            System.out.println((i + 1) + ". " + minions.get(i).toString());
        }
    }

    /**
     * Pre-populates the minions parameter with Minion objects.
     * @param minions List of minions.
     */
    private static void initializeMinions(List<Minion> minions){
        minions.add(new Minion("Bob",0.5));
        minions.add(new Minion("Stuart",0.7));
        minions.add(new Minion("Kevin",0.9));
    }
}
